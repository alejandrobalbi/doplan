const express = require('express');
const cors = require('cors')
const app = express();
const mongoose = require('mongoose')

const personModel = require('./person_schema')

app.use(express.urlencoded({ extended: false }));
 
app.use(express.json());

app.use(cors())
app.listen(3000, () => {
 console.log("Server started on port 3000")
})

mongoose.connect("mongodb://localhost:27017/doplan", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});
var db = mongoose.connection;
db.on('open', () => {
    console.log('Connected to mongoDB');
});
db.on('error', (error) => {
    console.log("---->"+error)
})

app.post('/person/add', (req, res) => {
    let newPerson = new personModel;
    newPerson.name = req.body.name
    newPerson.last_name = req.body.last_name
    newPerson.email = req.body.email
    newPerson.rol = req.body.rol
    newPerson.seniority = req.body.seniority
    newPerson.project = req.body.project
    newPerson.feedback = req.body.feedback
    newPerson.skills = req.body.skills
    newPerson.vacations = req.body.vacations
    newPerson.community_lead = req.body.community_lead
    newPerson.community = req.body.community
    newPerson.save((err) => {
      if(err){
        console.log(err)
        res.send("Error while adding Person");
      }else{
        res.send("Person added");
      }
    })
})

app.put('/people/:id', async (req, res) => {
  let filter = { _id: req.params.id }
  let update = req.body
  console.log(update)
  await personModel.findByIdAndUpdate(filter, update)
  res.send("Person updated")
})

app.get('/people', (req, res) => {
  personModel.find({}, (err, data) => {
    res.send(data);
  })
})

app.get('/people/:id', (req, res) => {
  personModel.find({ _id: req.params.id }, (err, data) => {
    res.send(data);
  })
})

app.delete('/people/:id', (req, res) => {
  personModel.deleteOne({ _id: req.params.id }, (err) => {
    if(err){
      res.send("Error while deleting todo")
    }else{
      res.send("Todo deleted")
    }
  })
})


/*
app.get('/todo/completed', (req, res) => {
    personModel.find({ completed: true }, (err, todos) => {
      if (err) {
        res.send("Error while fetching Todos");
      } else {
        res.json(todos)
      }
    })
})

app.get('/todo/uncompleted', (req, res) => {
    personModel.find({completed:false},(err, todos) => {
      if(err){
        res.send("Error while fetching Todos");
      }else{
        res.json(todos)
      }
    })
})

app.post('/todo/complete/:id',(req, res) => {
    personModel.findByIdAndUpdate(req.params.id, {completed: true},         (err, todo) =>{
      if(!err){
        res.send("Good Work");
      }
    })
})


*/
  