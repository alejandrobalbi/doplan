const mongoose = require("mongoose");
const personSchema = mongoose.Schema({
  name: {
    type: String,
    required: false
  }, 
  last_name: {
    type: String,
    required: false
  }, 
  email: {
    type: String,
    required: true
  },
  rol: {
    type: String,
    required: false
  },
  seniority: {
    type: String,
    required: false
  },
  project: {
    type: String,
    required: false
  },
  feedback: {
    type: String,
    required: false
  },
  skills: {
    type: String,
    required: false
  },
  vacations: {
    type: String,
    required: false
  },
  community_lead: {
    type: String,
    required: false
  },
  community: {
    type: String,
    required: false,
    default: "DevOps"
  }
});
const person = (module.exports = mongoose.model("person", personSchema));